public class Fibonacci {
    public static int fibonacciIterativo(int n) {
        if (n <= 1) {
            return n;
        }
        int fib = 1;
        int prevFib = 1;

        for (int i = 2; i < n; i++) {
            int temp = fib;
            fib += prevFib;
            prevFib = temp;
        }
        return fib;
    }

    public static int fibonacciRecursivo(int n) {
        if (n <= 1) {
            return n;
        }
        return fibonacciRecursivo(n-1) + fibonacciRecursivo(n-2);
    }

    public static void main(String[] args) {
        int n = 10; // el número de términos que deseamos calcular
        System.out.println("El " + n + "-ésimo término de la serie de Fibonacci (enfoque iterativo): " + fibonacciIterativo(n));
        System.out.println("El " + n + "-ésimo término de la serie de Fibonacci (enfoque recursivo): " + fibonacciRecursivo(n));
    }
}