import java.util.Arrays;

public class MaximoComunDivisor {
    public static int calcular(int[] numeros) {
        int mcd = numeros[0];
        for (int i = 1; i < numeros.length; i++) {
            mcd = calcular(mcd, numeros[i]);
        }
        return mcd;
    }

    public static int calcular(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calcular(b, a % b);
        }
    }

    public static void main(String[] args) {
        int[] numeros = { 12, 18, 24 };
        int mcd = calcular(numeros);
        System.out.println("El máximo común divisor de los números " + Arrays.toString(numeros) + " es " + mcd);
    }
}