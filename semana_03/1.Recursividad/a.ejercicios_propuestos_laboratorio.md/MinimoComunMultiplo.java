public class MinimoComunMultiplo {
    public static int calcular(int[] numeros) {
        int mcm = 1;
        int factor = 2;
        while (tieneDivisores(numeros)) {
            boolean divisible = false;
            for (int i = 0; i < numeros.length; i++) {
                if (numeros[i] % factor == 0) {
                    numeros[i] /= factor;
                    divisible = true;
                }
            }
            if (divisible) {
                mcm *= factor;
            } else {
                factor++;
            }
        }
        for (int i = 0; i < numeros.length; i++) {
            mcm *= numeros[i];
        }
        return mcm;
    }

    public static boolean tieneDivisores(int[] numeros) {
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] != 1) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[] numeros = { 12, 18, 24 };
        int mcm = calcular(numeros);
        System.out.println("El mínimo común múltiplo de los números " + Arrays.toString(numeros) + " es " + mcm);
    }

    @Override
    public String toString() {
        return "MinimoComunMultiplo []";
    }
}