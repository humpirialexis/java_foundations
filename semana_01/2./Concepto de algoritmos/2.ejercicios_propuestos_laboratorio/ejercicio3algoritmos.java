import java.util.Scanner;

public class ejercicio3algoritmos {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int numero1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int numero2 = scanner.nextInt();
        System.out.print("Ingrese el tercer número entero: ");
        int numero3 = scanner.nextInt();
        int maximo = Math.max(Math.max(numero1, numero2), numero3);
        System.out.println("El número mayor es: " + maximo);
        scanner.close();
    }
}