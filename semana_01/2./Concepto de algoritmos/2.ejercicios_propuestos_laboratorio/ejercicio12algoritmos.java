import java.util.Scanner;

public class ejercicio12algoritmos {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n, num, count = 0;

        System.out.print("Ingrese la cantidad de números: ");
        n = sc.nextInt();

        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese un número entero: ");
            num = sc.nextInt();

            if (num % 2 == 0) {
                count++;
            }
        }

        System.out.println("Hay " + count + " números pares en la lista.");
    }
}