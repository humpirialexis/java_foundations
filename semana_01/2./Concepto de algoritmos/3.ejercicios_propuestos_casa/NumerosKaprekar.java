public class NumerosKaprekar {
    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i <= 1000000; i++) {
            if (esNumeroKaprekar(i)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " números enteros que son números de Kaprekar.");
    }

    public static boolean esNumeroKaprekar(int numero) {
        int cuadrado = numero * numero;
        String cuadradoStr = Integer.toString(cuadrado);
        for (int i = 1; i < cuadradoStr.length(); i++) {
            String parte1Str = cuadradoStr.substring(0, i);
            String parte2Str = cuadradoStr.substring(i);
            int parte1 = Integer.parseInt(parte1Str);
            int parte2 = Integer.parseInt(parte2Str);
            if (parte1 > 0 && parte2 > 0 && parte1 + parte2 == numero) {
                return true;
            }
        }
        return false;
    }
}