public class NumerosArmstrong {
    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i <= 1000000; i++) {
            if (esNumeroArmstrong(i)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " números enteros que son números de Armstrong.");
    }

    public static boolean esNumeroArmstrong(int numero) {
        int suma = 0;
        int numeroOriginal = numero;
        int numDigitos = Integer.toString(numero).length();
        while (numero != 0) {
            int digito = numero % 10;
            suma += Math.pow(digito, numDigitos);
            numero /= 10;
        }
        return suma == numeroOriginal;
    }
}