import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PalabrasRepetidas6 {
    public static void main(String[] args) {
        int contador = 0;
        try {
            File archivo = new File("archivo.txt");
            Scanner scanner = new Scanner(archivo);
            while (scanner.hasNextLine()) {
                String linea = scanner.nextLine();
                String[] palabras = linea.split(" ");
                for (String palabra : palabras) {
                    if (tieneLetraRepetida(palabra.toLowerCase())) {
                        contador++;
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
        }
        System.out.println("Hay " + contador + " palabras que tienen una letra que aparece exactamente dos veces en cualquier posición.");
    }

    public static boolean tieneLetraRepetida(String palabra) {
        for (int i = 0; i < palabra.length(); i++) {
            char letra = palabra.charAt(i);
            int contador = 0;
            for (int j = 0; j < palabra.length(); j++) {
                if (palabra.charAt(j) == letra) {
                    contador++;
                }
            }
            if (contador == 2) {
                return true;
            }
        }
        return false;
    }
}