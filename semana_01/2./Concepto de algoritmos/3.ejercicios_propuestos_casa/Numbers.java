public class Numbers {
    public static void main(String[] args) {
        for (int i = 2; i <= 10000; i++) {
            if (esNumeroPerfecto(i)) {
                System.out.println(i + " es un número perfecto.");
            }
        }

        for (int i = 0; i <= 20; i++) {
            int numeroFibonacci = calcularFibonacci(i);
            System.out.println(numeroFibonacci + " es un número de Fibonacci.");
        }
    }

    public static boolean esNumeroPerfecto(int numero) {
        int sumaDivisores = 0;
        for (int i = 1; i < numero; i++) {
            if (numero % i == 0) {
                sumaDivisores += i;
            }
        }
        return sumaDivisores == numero;
    }

    public static int calcularFibonacci(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return calcularFibonacci(n - 1) + calcularFibonacci(n - 2);
        }
    }
}