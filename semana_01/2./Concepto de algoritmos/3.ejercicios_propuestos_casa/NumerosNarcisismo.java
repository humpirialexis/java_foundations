public class NumerosNarcisismo {
    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i <= 1000000; i++) {
            if (esNumeroNarcisismo(i)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " números enteros que son números de narcisismo.");
    }

    public static boolean esNumeroNarcisismo(int numero) {
        int sumaDigitos = 0;
        int numeroOriginal = numero;
        int numDigitos = Integer.toString(numero).length();
        while (numero != 0) {
            int digito = numero % 10;
            sumaDigitos += Math.pow(digito, numDigitos);
            numero /= 10;
        }
        return sumaDigitos == numeroOriginal;
    }
}