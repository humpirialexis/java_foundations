import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ContadorVocales {
    public static void main(String[] args) {
        int contador = 0;
        try {
            File archivo = new File("archivo.txt");
            Scanner scanner = new Scanner(archivo);
            while (scanner.hasNextLine()) {
                String linea = scanner.nextLine();
                String[] palabras = linea.split(" ");
                for (String palabra : palabras) {
                    if (contieneTodasLasVocales(palabra.toLowerCase())) {
                        contador++;
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
        }
        System.out.println("Hay " + contador + " palabras que contienen todas las vocales.");
    }

    public static boolean contieneTodasLasVocales(String palabra) {
        return palabra.contains("a") && palabra.contains("e") && palabra.contains("i") && palabra.contains("o") && palabra.contains("u");
    }
}