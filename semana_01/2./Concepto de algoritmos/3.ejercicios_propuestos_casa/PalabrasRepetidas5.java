import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PalabrasRepetidas5 {
    public static void main(String[] args) {
        int contador = 0;
        try {
            File archivo = new File("archivo.txt");
            Scanner scanner = new Scanner(archivo);
            while (scanner.hasNextLine()) {
                String linea = scanner.nextLine();
                String[] palabras = linea.split(" ");
                for (String palabra : palabras) {
                    if (tieneLetraRepetida(palabra.toLowerCase())) {
                        contador++;
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
        }
        System.out.println("Hay " + contador + " palabras que tienen la misma letra al principio y al final.");
    }

    public static boolean tieneLetraRepetida(String palabra) {
        if (palabra.length() >= 2 && palabra.charAt(0) == palabra.charAt(palabra.length() - 1)) {
            return true;
        } else {
            return false;
        }
    }
}