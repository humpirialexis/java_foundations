import java.util.Scanner;

public class SumaDePares {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número determinado: ");
        int n = sc.nextInt();
        for (int i = 1; i < n; i++) {
            for (int j = i + 1; j <= n; j++) {
                if (i + j == n) {
                    System.out.println("El par de números enteros cuya suma es igual a " + n + " es: " + i + " y " + j);
                    return;
                }
            }
        }
        System.out.println("No se encontró ningún par de números enteros cuya suma sea igual a " + n);
    }
}