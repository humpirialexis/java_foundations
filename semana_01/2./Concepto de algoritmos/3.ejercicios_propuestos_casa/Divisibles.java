public class Divisibles {
    public static void main(String[] args) {
        int count = 0;
        for (int i = 1; i <= 1000000; i++) {
            if (i % 1 == 0 && i % 2 == 0 && i % 3 == 0 && i % 4 == 0 && i % 5 == 0 && i % 6 == 0 && i % 7 == 0 && i % 8 == 0 && i % 9 == 0 && i % 10 == 0) {
                count++;
            }
        }
        System.out.println("Hay " + count + " números enteros divisibles por todos los números de 1 a 10.");
    }
}