public class iMain {
  public static void main(String[] args) {
    double numero1 = 3.1416;
    double raizCuadrada = Math.sqrt(numero1);
    double seno = Math.sin(numero1);
    double exponencial = Math.exp(numero1);
    System.out.println(raizCuadrada);
    System.out.println(seno);
    System.out.println(exponencial);
  }
}