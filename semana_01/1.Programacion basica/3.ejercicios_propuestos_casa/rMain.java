import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class rMain {
  public static void main(String[] args) {
    try {
      OutputStream flujoSalida = new FileOutputStream("archivo.txt");
      flujoSalida.write("Hola, mundo!".getBytes());
      flujoSalida.close();
    } catch (IOException e) {
      System.err.println("Error writing to file: " + e.getMessage());
    }
  }
}