import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class qMain {
  public static void main(String[] args) {
    try {
      InputStream flujoEntrada = new FileInputStream("archivo.txt");
      byte[] buffer = new byte;
      int numBytesLeidos = flujoEntrada.read(buffer);
      String datos = new String(buffer, 0, numBytesLeidos);
      System.out.println(datos);
      flujoEntrada.close();
    } catch (IOException e) {
      System.err.println("Error reading from file: " + e.getMessage());
    }
  }
}