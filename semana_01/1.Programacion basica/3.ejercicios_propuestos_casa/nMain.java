import java.math.BigDecimal;

public class nMain {
  public static void main(String[] args) {
    BigDecimal numero1 = new BigDecimal("1234567890.1234567890");
    BigDecimal numero2 = new BigDecimal("9876543210.9876543210");
    BigDecimal suma = numero1.add(numero2);
    System.out.println(suma);
  }
}