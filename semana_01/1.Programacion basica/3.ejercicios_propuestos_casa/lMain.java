import java.text.DateFormat;
import java.util.Date;

public class iMain {
  public static void main(String[] args) {
    DateFormat formato = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
    Date fechaHora = new Date();
    String fechaHoraFormateada = formato.format(fechaHora);
    System.out.println(fechaHoraFormateada);
  }
}