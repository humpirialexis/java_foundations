import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class kMain {
  public static void main(String[] args) {
    String cadena = "La casa es roja y la puerta es verde";
    Pattern patron = Pattern.compile("roja|verde");
    Matcher matcher = patron.matcher(cadena);
    while (matcher.find()) {
      System.out.println("Patrón encontrado en la posición " + matcher.start() + " - " + matcher.group());
    }
  }
}