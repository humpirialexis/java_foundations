import java.text.SimpleDateFormat;
import java.util.Date;

public class mMain {
  public static void main(String[] args) {
    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date fechaHora = new Date();
    String fechaHoraFormateada = formato.format(fechaHora);
    System.out.println(fechaHoraFormateada);
  }
}