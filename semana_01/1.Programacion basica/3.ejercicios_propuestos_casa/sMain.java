import java.net.Socket;
import java.io.IOException;

public class sMain {
  public static void main(String[] args) {
    try {
      Socket socket = new Socket("localhost", 1234);
      // Connection successful, do something with the socket...
      socket.close();
    } catch (IOException e) {
      System.err.println("Error connecting to server: " + e.getMessage());
    }
  }
}