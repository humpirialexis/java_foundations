import java.util.Scanner;

public class Diferencia {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Ingrese el primer número: ");
    int num1 = scanner.nextInt();
    System.out.print("Ingrese el segundo número: ");
    int num2 = scanner.nextInt();
    int diferencia = num1 - num2;
    System.out.println("La diferencia entre " + num1 + " y " + num2 + " es " + diferencia + ".");
  }
}