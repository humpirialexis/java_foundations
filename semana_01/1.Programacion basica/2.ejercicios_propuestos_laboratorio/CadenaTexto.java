import java.util.Scanner;

public class CadenaTexto {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Ingrese una cadena de texto: ");
    String cadena = scanner.nextLine();
    System.out.println("La cadena de texto ingresada es: " + cadena);
  }
}
