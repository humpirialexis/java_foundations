import java.util.Scanner;

public class MasaCorporal {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Ingrese su peso en kilogramos: ");
    double peso = scanner.nextDouble();
    System.out.print("Ingrese su altura en metros: ");
    double altura = scanner.nextDouble();
    double imc = peso / (altura * altura);
    System.out.println("Su índice de masa corporal (IMC) es: " + imc);
  }
}