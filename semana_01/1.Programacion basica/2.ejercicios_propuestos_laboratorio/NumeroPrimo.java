import java.util.Scanner;

public class NumeroPrimo {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Ingrese un número entero: ");
    int numero = scanner.nextInt();
    boolean esPrimo = true;
    for (int i = 2; i <= numero / 2; i++) {
      if (numero % i == 0) {
        esPrimo = false;
        break;
      }
    }
    if (esPrimo) {
      System.out.println("El número ingresado es primo.");
    } else {
      System.out.println("El número ingresado no es primo.");
    }
  }
}