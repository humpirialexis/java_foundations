import java.util.Scanner;

public class PerimetroPoligono {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        double lado, perimetro;

        System.out.print("Ingrese el número de lados del polígono: ");
        n = sc.nextInt();

        System.out.print("Ingrese la longitud de un lado del polígono: ");
        lado = sc.nextDouble();

        perimetro = n * lado;

        System.out.println("El perímetro del polígono es: " + perimetro);
    }
}