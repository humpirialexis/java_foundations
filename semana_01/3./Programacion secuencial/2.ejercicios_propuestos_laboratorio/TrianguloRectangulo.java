import java.util.Scanner;

public class TrianguloRectangulo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double a, b, c;

        System.out.print("Ingrese el lado a: ");
        a = sc.nextDouble();

        System.out.print("Ingrese el lado b: ");
        b = sc.nextDouble();

        System.out.print("Ingrese el lado c: ");
        c = sc.nextDouble();

        if (a + b > c && a + c > b && b + c > a) {
            if (a * a + b * b == c * c || a * a + c * c == b * b || b * b + c * c == a  * a) {
                System.out.println("Los lados ingresados forman un triángulo rectángulo.");

                double senA = a / c;
                double senB = b / c;
                double cosA = b / c;
                double cosB = a / c;
                double tanA = a / b;
                double tanB = b / a;

                System.out.println("Seno de A: " + senA);
                System.out.println("Seno de B: " + senB);
                System.out.println("Coseno de A: " + cosA);
                System.out.println("Coseno de B: " + cosB);
                System.out.println("Tangente de A: " + tanA);
                System.out.println("Tangente de B: " + tanB);
            } else {
                System.out.println("Los lados ingresados no forman un triángulo rectángulo.");
            }
        } else {
            System.out.println("Los lados ingresados no forman un triángulo.");
        }
    }
}