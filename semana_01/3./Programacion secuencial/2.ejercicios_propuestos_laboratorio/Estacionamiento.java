import java.util.Scanner;

public class Estacionamiento {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int horas, minutos;
        double tiempoEstacionamiento, costoEstacionamiento;

        System.out.print("Ingrese las horas de estacionamiento: ");
        horas = sc.nextInt();

        System.out.print("Ingrese los minutos de estacionamiento: ");
        minutos = sc.nextInt();

        tiempoEstacionamiento = horas + (minutos / 60.0);
        costoEstacionamiento = tiempoEstacionamiento * 2.5;

        System.out.println("El tiempo de estacionamiento es: " + tiempoEstacionamiento + " horas");
        System.out.println("El costo de estacionamiento es: S/." + costoEstacionamiento);
    }
}