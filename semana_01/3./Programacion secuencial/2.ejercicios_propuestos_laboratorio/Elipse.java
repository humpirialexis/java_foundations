import java.util.Scanner;

public class Elipse {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double a, b, area, perimetro;

        System.out.print("Ingrese el valor del semieje mayor: ");
        a = sc.nextDouble();

        System.out.print("Ingrese el valor del semieje menor: ");
        b = sc.nextDouble();

        area = Math.PI * a * b;
        perimetro = 2 * Math.PI * Math.sqrt((a * a + b * b) / 2);

        System.out.println("El área de la elipse es: " + area);
        System.out.println("El perímetro de la elipse es: " + perimetro);
    }
}