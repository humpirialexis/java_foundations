import java.util.Scanner;

public class RaizCuadrada {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;

        System.out.print("Ingrese un número entero: ");
        num = sc.nextInt();

        double raiz = Math.sqrt(num);

        System.out.println("La raíz cuadrada de " + num + " es: " + raiz);
    }
}