import java.util.Scanner;

public class ValorAbsoluto {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;

        System.out.print("Ingrese un número entero: ");
        num = sc.nextInt();

        int valorAbsoluto = Math.abs(num);

        System.out.println("El valor absoluto de " + num + " es: " + valorAbsoluto);
    }
}