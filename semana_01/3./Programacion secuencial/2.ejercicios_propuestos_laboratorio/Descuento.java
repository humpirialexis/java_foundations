import java.util.Scanner;

public class Descuento {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double precioOriginal, precioFinal;

        System.out.print("Ingrese el precio original de la compra: ");
        precioOriginal = sc.nextDouble();

        precioFinal = precioOriginal - (precioOriginal * 0.15);

        System.out.println("El precio final con el descuento del 15% es: S/." + precioFinal);
    }
}