import java.util.Scanner;

public class PresupuestoHospital {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double presupuestoAnual;

        System.out.print("Ingrese el presupuesto anual del hospital: ");
        presupuestoAnual = sc.nextDouble();

        double presupuestoOncologia = presupuestoAnual * 0.55;
        double presupuestoPediatría = presupuestoAnual * 0.20;
        double presupuestoTraumatología = presupuestoAnual * 0.25;

        System.out.println("El presupuesto asignado a Oncología es: S/" + presupuestoOncologia);
        System.out.println("El presupuesto asignado a Pediatría es: S/" + presupuestoPediatría);
        System.out.println("El presupuesto asignado a Traumatología es: S/" + presupuestoTraumatología);
    }
}