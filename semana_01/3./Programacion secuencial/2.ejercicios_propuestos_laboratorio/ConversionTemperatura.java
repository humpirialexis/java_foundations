import java.util.Scanner;

public class ConversionTemperatura {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double temperaturaCelsius, temperaturaFahrenheit;

        System.out.print("Ingrese la temperatura en grados Celsius: ");
        temperaturaCelsius = sc.nextDouble();

        temperaturaFahrenheit = (9 * temperaturaCelsius / 5) + 32;

        System.out.println("La temperatura en grados Fahrenheit es: " + temperaturaFahrenheit + "°F");
    }
}