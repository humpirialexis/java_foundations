import java.util.Scanner;

public class AreaTrapecio {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double lado1, lado2, altura, area;

        System.out.print("Ingrese la longitud de la base mayor: ");
        lado1 = sc.nextDouble();

        System.out.print("Ingrese la longitud de la base menor: ");
        lado2 = sc.nextDouble();

        System.out.print("Ingrese la altura del trapecio: ");
        altura = sc.nextDouble();

        area = ((lado1 + lado2) / 2) * altura;

        System.out.println("El área del trapecio es: " + area);
    }
}