import java.util.Scanner;

public class SalarioVendedor {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double salarioBase = 2500.00;
        double comisionEquipo = 50.00;
        double porcentajeVenta = 0.07;

        System.out.print("Ingrese el nombre del vendedor: ");
        String nombre = sc.nextLine();

        System.out.print("Ingrese el número de equipos exclusivos vendidos: ");
        int equiposExclusivos = sc.nextInt();

        System.out.print("Ingrese el valor total facturado: ");
        double valorFacturado = sc.nextDouble();

        double comisionVenta = porcentajeVenta * valorFacturado;
        double salarioTotal = salarioBase + (comisionEquipo * equiposExclusivos) + comisionVenta;

        System.out.println("El salario total de " + nombre + " es: S/" + salarioTotal);
    }
}