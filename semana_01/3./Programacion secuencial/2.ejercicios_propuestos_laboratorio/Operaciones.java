import java.util.Scanner;

public class Operaciones {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double num1, num2, resultado;
        char operacion;

        System.out.print("Ingrese el primer número: ");
        num1 = sc.nextDouble();

        System.out.print("Ingrese el segundo número: ");
        num2 = sc.nextDouble();

        System.out.print("Ingrese la operación a realizar (+,-,*,/,^): ");
        operacion = sc.next().charAt(0);

        switch (operacion) {
            case '+':
                resultado = num1 + num2;
                System.out.println("El resultado de la suma es: " + resultado);
                break;
            case '-':
                resultado = num1 - num2;
                System.out.println("El resultado de la resta es: " + resultado);
                break;
            case '*':
                resultado = num1 * num2;
                System.out.println("El resultado de la multiplicación es: " + resultado);
                break;
            case '/':
                resultado = num1 / num2;
                System.out.println("El resultado de la división es: " + resultado);
                break;
            case '^':
                resultado = Math.pow(num1, num2);
                System.out.println("El resultado de la potencia es: " + resultado);
                break;
            default:
                System.out.println("Operación no válida");
                break;
        }
    }
}