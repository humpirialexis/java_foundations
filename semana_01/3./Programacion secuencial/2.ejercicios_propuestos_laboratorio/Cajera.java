import java.util.Scanner;

public class Cajera {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int cantidad, billetes200, billetes100, billetes50, billetes20, billetes10, billetes5, billetes2, billetes1;

        System.out.print("Ingrese la cantidad: ");
        cantidad = sc.nextInt();

        billetes200 = cantidad / 200;
        cantidad %= 200;

        billetes100 = cantidad / 100;
        cantidad %= 100;

        billetes50 = cantidad / 50;
        cantidad %= 50;

        billetes20 = cantidad / 20;
        cantidad %= 20;

        billetes10 = cantidad / 10;
        cantidad %= 10;

        billetes5 = cantidad / 5;
        cantidad %= 5;

        billetes2 = cantidad / 2;
        cantidad %= 2;

        billetes1 = cantidad;

        System.out.println("Se necesitan " + billetes200 + " billetes de 200");
        System.out.println("Se necesitan " + billetes100 + " billetes de 100");
        System.out.println("Se necesitan " + billetes50 + " billetes de 50");
        System.out.println("Se necesitan " + billetes20 + " billetes de 20");
        System.out.println("Se necesitan " + billetes10 + " billetes de 10");
        System.out.println("Se necesitan " + billetes5 + " billetes de 5");
        System.out.println("Se necesitan " + billetes2 + " monedas de 2");
        System.out.println("Se necesitan " + billetes1 + " monedas de 1");
    }
}