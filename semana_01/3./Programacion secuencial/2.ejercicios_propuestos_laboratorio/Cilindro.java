import java.util.Scanner;

public class Cilindro {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double radio, altura, areaSuperficie, volumen;

        System.out.print("Ingrese el radio del cilindro: ");
        radio = sc.nextDouble();

        System.out.print("Ingrese la altura del cilindro: ");
        altura = sc.nextDouble();

        areaSuperficie = 2 * Math.PI * radio * altura;
        volumen = Math.PI * Math.pow(radio, 2) * altura;

        System.out.println("El área de la superficie lateral del cilindro es: " + areaSuperficie);
        System.out.println("El volumen del cilindro es: " + volumen);
    }
}