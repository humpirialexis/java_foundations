import java.util.Scanner;

public class Hipotenusa {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double cateto1, cateto2, hipotenusa;

        System.out.print("Ingrese el valor del primer cateto: ");
        cateto1 = sc.nextDouble();

        System.out.print("Ingrese el valor del segundo cateto: ");
        cateto2 = sc.nextDouble();

        hipotenusa = Math.sqrt(cateto1 * cateto1 + cateto2 * cateto2);

        System.out.println("El valor de la hipotenusa es: " + hipotenusa);
    }
}