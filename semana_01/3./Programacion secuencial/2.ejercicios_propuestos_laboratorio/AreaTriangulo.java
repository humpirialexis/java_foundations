import java.util.Scanner;

public class AreaTriangulo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double lado1, lado2, lado3, semiperimetro, area;

        System.out.print("Ingrese el valor del primer lado: ");
        lado1 = sc.nextDouble();

        System.out.print("Ingrese el valor del segundo lado: ");
        lado2 = sc.nextDouble();

        System.out.print("Ingrese el valor del tercer lado: ");
        lado3 = sc.nextDouble();

        semiperimetro = (lado1 + lado2 + lado3) / 2;
        area = Math.sqrt(semiperimetro * (semiperimetro - lado1) * (semiperimetro - lado2) * (semiperimetro - lado3));

        System.out.println("El área del triángulo es: " + area);
    }
}

