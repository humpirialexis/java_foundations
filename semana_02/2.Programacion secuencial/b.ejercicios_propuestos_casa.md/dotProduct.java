public static int dotProduct(int[] a, int[] b) {
    int result = 0;
    for (int i = 0; i < a.length; i++) {
        result += a[i] * b[i];
    }
    return result;
}

int[] a = {1, 2, 3};
int[] b = {4, 5, 6};