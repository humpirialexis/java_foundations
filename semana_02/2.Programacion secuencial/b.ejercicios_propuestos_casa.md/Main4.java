import java.util.Arrays;

public class Main4 {
    public static void main(String[] args) {
        int[] arr = {5, 2, 8, 3, 1, 4};
        int k = 3;

        Arrays.sort(arr); // Ordenar el arreglo en orden ascendente

        int kthLargest = arr[arr.length - k]; // Encontrar el k-ésimo elemento más grande

        System.out.println("El " + k + "-ésimo elemento más grande es: " + kthLargest);
    }
}