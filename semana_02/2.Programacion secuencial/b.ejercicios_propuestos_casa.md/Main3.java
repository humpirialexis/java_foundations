public static void Main3(String[] args) {
    int[] arr = {2, 3, 4, 5, 3, 6, 7, 3, 8, 9, 3};
    Map<Integer, Integer> frequencyMap = new HashMap<>();
    for (int i = 0; i < arr.length; i++) {
        if (frequencyMap.containsKey(arr[i])) {
            frequencyMap.put(arr[i], frequencyMap.get(arr[i]) + 1);
        } else {
            frequencyMap.put(arr[i], 1);
        }
    }
    int maxValue = 0;
    int maxKey = 0;
    for (Map.Entry<Integer, Integer> entry : frequencyMap.entrySet()) {
        if (entry.getValue() > maxValue) {
            maxValue = entry.getValue();
            maxKey = entry.getKey();
        }
    }
    System.out.println("La moda es: " + maxKey);
}