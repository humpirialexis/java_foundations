public class Main {
    public static void main(String[] args) {
        int[][] matriz = new int;
        // Inicializar la matriz con valores
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                matriz[i][j] = i * 7 + j + 1;
            }
        }
        // Imprimir la matriz original
        System.out.println("Matriz original:");
        imprimirMatriz(matriz);
        // Rotar la matriz 90 grados en sentido horario
        for (int i = 0; i < 7 / 2; i++) {
            for (int j = i; j < 7 - i - 1; j++) {
                int temp = matriz[i][j];
                matriz[i][j] = matriz[7 - j - 1][i];
                matriz[7 - j - 1][i] = matriz[7 - i - 1][7 - j - 1];
                matriz[7 - i - 1][7 - j - 1] = matriz[j][7 - i - 1];
                matriz[j][7 - i - 1] = temp;
            }
        }
        // Imprimir la matriz rotada
        System.out.println("Matriz rotada 90 grados en sentido horario:");
        imprimirMatriz(matriz);
    }
    public static void imprimirMatriz(int[][] matriz) {
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}