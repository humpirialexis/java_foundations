import java.util.HashMap;

public class Main5 {
  public static void main(String[] args) {
    int[] arr = {1, 2, 3, 4, 5, 2, 3, 2, 2};
    HashMap<Integer, Integer> freq = new HashMap<Integer, Integer>();

    for (int i = 0; i < arr.length; i++) {
      if (freq.containsKey(arr[i])) {
        freq.put(arr[i], freq.get(arr[i]) + 1);
      } else {
        freq.put(arr[i], 1);
      }
    }

    int maxFreq = 0;
    int maxNum = 0;

    for (int num : freq.keySet()) {
      if (freq.get(num) > maxFreq) {
        maxFreq = freq.get(num);
        maxNum = num;
      }
    }

    System.out.println("El número que se repite más veces es: " + maxNum);
  }
}