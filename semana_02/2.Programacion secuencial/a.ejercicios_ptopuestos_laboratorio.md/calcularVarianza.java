public static double calcularVarianza(int[] arreglo) {
    double media = 0;
    double sumatoria = 0;
    int n = arreglo.length;

    // Calcular la media aritmética
    for (int i = 0; i < n; i++) {
        media += arreglo[i];
    }
    media /= n;

    // Calcular la sumatoria de (xi - media)^2
    for (int i = 0; i < n; i++) {
        sumatoria += Math.pow(arreglo[i] - media, 2);
    }

    // Calcular la varianza
    double varianza = sumatoria / n;

    return varianza;
}