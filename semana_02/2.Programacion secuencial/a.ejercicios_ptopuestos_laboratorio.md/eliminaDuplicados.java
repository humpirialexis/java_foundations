public static int[] eliminarDuplicados(int[] arreglo) {
    int[] nuevoArreglo = new int[arreglo.length];
    int indice = 0;
    for (int i = 0; i < arreglo.length; i++) {
        boolean duplicado = false;
        for (int j = 0; j < indice; j++) {
            if (arreglo[i] == nuevoArreglo[j]) {
                duplicado = true;
                break;
            }
        }
        if (!duplicado) {
            nuevoArreglo[indice] = arreglo[i];
            indice++;
        }
    }
    int[] resultado = new int[indice];
    for (int i = 0; i < indice; i++) {
        resultado[i] = nuevoArreglo[i];
    }
    return resultado;
}