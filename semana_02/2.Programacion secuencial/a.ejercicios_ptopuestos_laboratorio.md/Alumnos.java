import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Alumnos {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> nombres = new ArrayList<>();
        ArrayList<Integer> edades = new ArrayList<>();
        String nombre;
        int edad;

        do {
            System.out.print("Ingrese el nombre del alumno (o * para salir): ");
            nombre = sc.nextLine();
            if (!nombre.equals("*")) {
                System.out.print("Ingrese la edad del alumno: ");
                edad = sc.nextInt();
                sc.nextLine(); // Consumir el salto de línea después de la edad
                nombres.add(nombre);
                edades.add(edad);
            }
        } while (!nombre.equals("*"));

        // Mostrar todos los alumnos mayores de edad
        System.out.println("Alumnos mayores de edad:");
        for (int i = 0; i < nombres.size(); i++) {
            if (edades.get(i) >= 18) {
                System.out.println(nombres.get(i));
            }
        }

        // Encontrar el alumno más viejo
        int indiceMayor = edades.indexOf(Collections.max(edades));
        System.out.println("El alumno más viejo es " + nombres.get(indiceMayor) + " con " + edades.get(indiceMayor) + " años.");
    }
}