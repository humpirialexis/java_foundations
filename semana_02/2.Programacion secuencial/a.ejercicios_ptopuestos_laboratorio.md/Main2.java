import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese un número entre 1 y 7: ");
        int num = scanner.nextInt();

        switch (num) {
            case 1:
                System.out.println("Hoy aprenderemos sobre programación");
                break;
            case 2:
                System.out.println("¿Qué tal tomar un curso de marketing digital?");
                break;
            case 3:
                System.out.println("Hoy es un gran día para comenzar a aprender de diseño");
                break;
            case 4:
                System.out.println("¿Y si aprendemos algo de negocios online?");
                break;
            case 5:
                System.out.println("Veamos un par de clases sobre producción audiovisual");
                break;
            case 6:
                System.out.println("Tal vez sea bueno desarrollar una habilidad blanda");
                break;
            case 7:
                System.out.println("Yo decido distraerme programando");
                break;
            default:
                System.out.println("El número ingresado no está en el rango válido.");
                break;
        }
    }
}