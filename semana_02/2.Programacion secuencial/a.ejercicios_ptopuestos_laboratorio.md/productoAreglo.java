public static int productoArreglo(int[] arreglo) {
    int producto = 1;
    for (int i = 0; i < arreglo.length; i++) {
        producto *= arreglo[i];
    }
    return producto;
}

int[] arreglo = {1, 2, 3, 4, 5};
int producto = productoArreglo(arreglo);
System.out.println("El producto de los elementos del arreglo es: " + producto);