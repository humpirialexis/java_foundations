import java.util.Scanner;

public class RomanNumerals {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese un número entre 1 y 3999: ");
        int num = input.nextInt();
        if (num < 1 || num > 3999) {
            System.out.println("Número inválido");
            return;
        }
        String[] roman = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        int[] decimal = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < roman.length; i++) {
            while (num >= decimal[i]) {
                result.append(roman[i]);
                num -= decimal[i];
            }
        }
        System.out.println("El número en números romanos es: " + result.toString());
    }
}