public static int encontrarMaximo(int[] arreglo) {
    int maximo = arreglo;
    for (int i = 1; i < arreglo.length; i++) {
        if (arreglo[i] > maximo) {
            maximo = arreglo[i];
        }
    }
    return maximo;
}

int[] arreglo = {5, 2, 8, 3, 1, 4};
int maximo = encontrarMaximo(arreglo);
System.out.println("El valor máximo en el arreglo es: " + maximo);