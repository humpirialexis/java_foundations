import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] results = new String[1];

        System.out.print("¿Está lloviendo? (sí/no): ");
        String answer = scanner.nextLine().toLowerCase();
        if (answer.equals("sí")) {
            System.out.print("¿Está haciendo mucho viento? (sí/no): ");
            answer = scanner.nextLine().toLowerCase();
            if (answer.equals("sí")) {
                results = "Hace mucho viento para salir con una sombrilla.";
            } else {
                results = "Lleva una sombrilla por si acaso.";
            }
        } else {
            results = "Que tengas un bonito día.";
        }

        System.out.println(results);
    }
}