import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Definir la matriz y llenarla con los valores ingresados por el usuario
        System.out.print("Ingrese el número de filas: ");
        int filas = scanner.nextInt();
        System.out.print("Ingrese el número de columnas: ");
        int columnas = scanner.nextInt();
        int[][] matriz = new int[filas][columnas];
        System.out.println("Ingrese los elementos de la matriz:");
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                matriz[i][j] = scanner.nextInt();
            }
        }

        // Ordenar la matriz utilizando el método de inserción
        for (int i = 0; i < filas; i++) {
            for (int j = 1; j < columnas; j++) {
                int actual = matriz[i][j];
                int k = j - 1;
                while (k >= 0 && matriz[i][k] > actual) {
                    matriz[i][k + 1] = matriz[i][k];
                    k--;
                }
                matriz[i][k + 1] = actual;
            }
        }

        // Imprimir la matriz ordenada
        System.out.println("Matriz ordenada:");
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}