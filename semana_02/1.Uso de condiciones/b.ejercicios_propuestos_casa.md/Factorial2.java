public class Factorial2 {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println("Factorial of " + i + " is: " + factorial(i));
        }
    }

    public static int factorial(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}