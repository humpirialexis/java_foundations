import java.util.Scanner;

public class CompraPlazos {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double total = 0;
        int meses = 20;
        double pagoMensual = 0;
        System.out.print("Ingrese el monto total de la compra: ");
        double monto = sc.nextDouble();
        for (int i = 1; i <= meses; i++) {
            pagoMensual = monto / Math.pow(2, i - 1);
            total += pagoMensual;
            System.out.printf("Mes %d: Pago mensual = S/%.2f\n", i, pagoMensual);
        }
        System.out.printf("Total pagado después de %d meses: S/%.2f\n", meses, total);
    }
}