public class LoanRepaymentSchedule {
    public static void main(String[] args) {
        double loanAmount = 5000;
        double interestRate = 0.016;
        int repaymentPeriod = 18;
        double monthlyInterestRate = interestRate / 12;
        double monthlyPayment = loanAmount * monthlyInterestRate / (1 - Math.pow(1 + monthlyInterestRate, -repaymentPeriod));
        double balance = loanAmount;
        System.out.println("Month\tPayment\t\tInterest\tPrincipal\tBalance");
        for (int i = 1; i <= repaymentPeriod; i++) {
            double interest = balance * monthlyInterestRate;
            double principal = monthlyPayment - interest;
            balance -= principal;
            System.out.printf("%d\t%.2f\t\t%.2f\t\t%.2f\t\t%.2f\n", i, monthlyPayment, interest, principal, balance);
        }
    }
}