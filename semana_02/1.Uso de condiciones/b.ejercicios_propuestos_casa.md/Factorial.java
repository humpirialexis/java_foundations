public class Factorial {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Uso: java Factorial <numero>");
            return;
        }

        int n = Integer.parseInt(args);
        int factorial = 1;

        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }

        System.out.println("El factorial de " + n + " es " + factorial);
    }
}