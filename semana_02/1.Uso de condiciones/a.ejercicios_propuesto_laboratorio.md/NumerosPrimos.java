import java.util.Scanner;

public class NumerosPrimos {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número hasta el que desea mostrar los números primos: ");
        n = sc.nextInt();
        System.out.println("Los números primos desde 2 hasta " + n + " son: ");
        for (int i = 2; i <= n; i++) {
            if (esPrimo(i)) {
                System.out.print(i + " ");
            }
        }
    }

    public static boolean esPrimo(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}