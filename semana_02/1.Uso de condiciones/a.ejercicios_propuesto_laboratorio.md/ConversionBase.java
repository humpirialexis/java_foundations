import java.util.Scanner;

public class ConversionBase {
    public static void main(String[] args) {
        int num, base;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número en base decimal que desea convertir: ");
        num = sc.nextInt();
        System.out.print("Ingrese la base a la que desea convertir el número: ");
        base = sc.nextInt();
        System.out.println("El número " + num + " en base " + base + " es: " + convertirBase(num, base));
    }

    public static String convertirBase(int num, int base) {
        String conversion = "";
        while (num > 0) {
            int residuo = num % base;
            if (residuo < 10) {
                conversion = residuo + conversion;
            } else {
                conversion = (char) (residuo - 10 + 'A') + conversion;
            }
            num /= base;
        }
        return conversion;
    }
}