import java.util.Scanner;

public class MaximoComunDivisor {
    public static void main(String[] args) {
        int n, mcd, i;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números: ");
        n = sc.nextInt();
        System.out.print("Ingrese el primer número: ");
        mcd = sc.nextInt();
        i = 2;
        while (i <= n) {
            System.out.print("Ingrese el siguiente número: ");
            int num = sc.nextInt();
            mcd = calcularMCD(mcd, num);
            i++;
        }
        System.out.println("El MCD de los números ingresados es: " + mcd);
    }

    public static int calcularMCD(int a, int b) {
        int resto;
        while (b != 0) {
            resto = a % b;
            a = b;
            b = resto;
        }
        return a;
    }
}