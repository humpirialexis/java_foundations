import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        int n, t1 = 0, t2 = 1;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de términos que desea imprimir: ");
        n = sc.nextInt();
        System.out.print("Serie de Fibonacci hasta " + n + " términos: ");

        while (t1 <= n) {
            System.out.print(t1 + " + ");
            int sum = t1 + t2;
            t1 = t2;
            t2 = sum;
        }
    }
}