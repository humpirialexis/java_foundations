import java.util.Scanner;

public class SumaNumerosImpares {
    public static void main(String[] args) {
        int n, sum = 0, count = 1;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de números impares que desea sumar: ");
        n = sc.nextInt();

        while (count <= n) {
            sum += 2 * count - 1;
            count++;
        }

        System.out.println("La suma de los " + n + " primeros números impares es: " + sum);
    }
}