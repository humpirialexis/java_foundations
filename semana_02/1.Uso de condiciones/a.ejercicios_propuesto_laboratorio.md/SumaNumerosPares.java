import java.util.Scanner;

public class SumaNumerosPares {
    public static void main(String[] args) {
        int n, sum = 0, count = 0, num = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de números pares que desea sumar: ");
        n = sc.nextInt();

        do {
            num += 2;
            sum += num;
            count++;
        } while (count < n);

        System.out.println("La suma de los " + n + " primeros números pares es: " + sum);
    }
}