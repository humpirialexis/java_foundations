import java.util.Scanner;

public class SumaNumerosPerfectos {
    public static void main(String[] args) {
        int n, count = 0, num = 1, sum = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de números perfectos que desea sumar: ");
        n = sc.nextInt();

        while (count < n) {
            if (esPerfecto(num)) {
                sum += num;
                count++;
            }
            num++;
        }

        System.out.println("La suma de los " + n + " primeros números perfectos es: " + sum);
    }

    public static boolean esPerfecto(int num) {
        int suma = 0;
        for (int i = 1; i < num; i++) {
            if (num % i == 0) {
                suma += i;
            }
        }
        return suma == num;
    }
}